/********************************************************************\

  Name:         spi.c
  Created by:   Stefan Ritt

  Contents:     SPI interface

\********************************************************************/

#include <stdio.h>
#include <string.h>
#include <intrins.h>
#include <stdlib.h>
#include "mscbemb.h"

/*------------------------------------------------------------------*/

sbit SPI_CLK      = P0 ^ 0;
sbit SPI_DI       = P0 ^ 1;
sbit SPI_DO       = P0 ^ 2;
sbit SPI_CE       = P0 ^ 3;

void spi_init()
{
   SFRPAGE = LEGACY_PAGE;
   
   SPI_CLK      = 0;
   SPI_DO       = 0;
   SPI_CE       = 1;
}

void spi_adr()
{
   SFRPAGE = LEGACY_PAGE;
   SPI_CE  = 0;
}

void spi_deadr()
{
   SFRPAGE = LEGACY_PAGE;
   SPI_CE  = 1;
}

void spi_write_msb(unsigned char d)
/* write one byte to SPI, LSB first */
{
   unsigned char i;

   SFRPAGE = LEGACY_PAGE;
  
   for (i=0 ; i<8 ; i++) {
      SPI_DO = (d & 0x80) > 0;
      d <<= 1;
      SPI_CLK = 1;
      SPI_CLK = 1;
      SPI_CLK = 1; // make pulse 120 ns wide
      SPI_CLK = 0;
   }
   SPI_DO = 0;
}

unsigned char spi_read_msb()
{
   unsigned char i;
   unsigned char b;
   
   SFRPAGE = LEGACY_PAGE;
   b = 0;
   
   SPI_DO = 0;
   for (i=0 ; i<8 ; i++) {
      b = (b << 1) | SPI_DI;
      SPI_CLK = 1;
      SPI_CLK = 1;
      SPI_CLK = 1; // make pulse 120 ns wide
      SPI_CLK = 0;
   }
   
   return b;
}
