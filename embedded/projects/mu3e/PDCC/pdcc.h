/********************************************************************\

  Name:         pdcc.h
  Created by:   Frederik Wauters 

  Contents:     pdcc.h header file, defining constants and utility functions

\********************************************************************/


#define N_SLOTS 16

//
// DC-DC board constants
//
#define TPS_ADDR        0x1B
#define AD5593_ADDR     0x10
#define AD5593_ADDR_2   0x11

// AD5593 registers and io pins

#define AD5593_ CONFIG_MODE_POINTER 0x00
#define AD5593_DAC_WR_POINTER 0x10
#define AD5593_ADC_RD_POINTER 0x40
#define AD5593_DAC_RD_POINTER 0x50
#define AD5593_GPIO_RD_POINTER 0x60
#define AD5593_REG_RD_POINTER 0x70 

#define AD5593_ADC_SEQ_REG 0x02
#define AD5593_GEN_CTRL_REG 0x03
#define AD5593_ADC_CONFIG_REG 0x04
#define AD5593_DAC_CONFIG_REG 0x05
#define AD5593_PULLDWN_CONFIG_REG  0x06
#define AD5593_LDAC_MODE_REG 0x07
#define AD5593_GPIO_CONFIG_REG  0x08 // have to be explicit here, as the crate controller also has a gpio
#define AD5593_GPIO_OUTPUT_REG  0x09
#define AD5593_GPIO_INPUT_REG 0x0A
#define AD5593_PD_REF_CTRL 0x0B
#define AD5593_GPIO_OPENDRAIN_REG 0x0C
#define AD5593_IO_TS_CONFIG_REG 0x0D
#define AD5593_SW_RESTE_REG 0x0F
#define AD5593_DAC_WR_rEG 0x10



#define PGOOD_PIN           0x40 //pin I/06
#define PGOOD_PIN_POS       6
#define INTERLOCK_COMP_PIN  0x04 // Interlock comparator pin I/02
#define INTERLOCK_COMP_PIN_POS  2
#define LED_PIN             0x80 // pin I/07
#define EN_PIN              0x20 // pin I/05

#define VDIODE_PIN          0x01 //pin I/00
#define VTHRESH_PIN         0x02 // pin I/01
#define VMUPIX_PIN          0x08 // pin I/03
#define CURRENT_PIN         0x10 // pin I/04



// TPS53819A registers and stuff

#define TPS_FREQUENCY_CONFIG_REG    0xD3
#define TPS_ON_OFF_CONFIG_REG       0x02
#define TPS_WRITE_PROTECT_REG       0x10
#define TPS_CLEAR_FAULTS_REG        0x03
#define TPS_STATUS_WORD_REG         0x79
#define TPS_MODE_SOFT_START_CONFIG_REG  0xD2
#define TPS_VOUT_ADJUSTMENT_REG    0xD4
#define TPS_VOUT_MARGIN_REG    0xD5
#define TPS_UVLO_THRESHOLD_REG 0xD6
#define TPS_OPERATION_REG      0x01

#define TPS_FREQUENCY_1MHZ          0x07

#define TPS_CML_POS 9
#define TPS_TEMP_POS 10
#define TPS_VIN_UV_POS 11
#define TPS_IOUT_OC_POS 12
#define TPS_VOUT_OV_POS 13
#define TPS_OFF_POS 14

// other
#define NUM_READINGS 7  // Number of ADC readings

////
//// DC-DC utility functions
////
unsigned char slot_to_pin(unsigned char slot);
unsigned short check_presence();
unsigned char config_AD5933();
unsigned char config_AD5933_Sense();
unsigned char config_TPS53819A();
float read_adc(unsigned short pin);
float read_adc_temp(unsigned short pin);
float read_adc_filtered(unsigned short pin);
unsigned short get_bit(unsigned short word, unsigned char position);
unsigned short set_bit(unsigned short word, unsigned char position, unsigned short value);
unsigned char allowed_to_turn_on(unsigned char ch);
unsigned char trim_vout(float trim_percentage);
unsigned char get_vout_adj_code(float value);
unsigned char get_vout_margin_code(float value);
