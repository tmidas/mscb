/********************************************************************\

  Name:         DS28EA00.c
  Created by:   Stefan Ritt

  Contents:     Driver for DS28EA00 1-wire temperature sensor

\********************************************************************/

#include <stdio.h>
#include <string.h>
#include <intrins.h>
#include <stdlib.h>
#include "mscbemb.h"
#include "DS28EA00.h"

/* DS28EA00 command codes */

#define CMD_WRITE_SCRATCHPAD     0x4E
#define CMD_READ_SCRATCHPAD      0xBE
#define CMD_COPY_SCRATCHPAD      0x48
#define CMD_CONVERT_TEMP         0x44
#define CMD_READ_POWER_MODE      0xB4
#define CMD_RECALL_EEPROM        0xB8
#define CMD_PIO_ACCESS_READ      0xF5
#define CMD_PIO_ACCESS_WRITE     0xA5

#define CMD_CHAIN                0x99
#define CMD_CHAIN_OFF            0x3C
#define CMD_CHAIN_OFF_INV        0xC3
#define CMD_CHAIN_ON             0x5A
#define CMD_CHAIN_ON_INV         0xA5
#define CMD_CHAIN_DONE           0x96
#define CMD_CHAIN_DONE_INV       0x69

#define CMD_READ_ROM             0x33
#define CMD_MATCH_ROM            0x55
#define CMD_SEARCH_ROM           0xF0
#define CMD_COND_SEARCH_ROM      0xEC
#define CMD_COND_READ_ROM        0x0F
#define CMD_SKIP_ROM             0xCC

#define CMD_OVERDRIVE_SKIP_ROM   0x3C
#define CMD_OVERDRIVE_MATCH_ROM  0x69

// port pins
sbit DS28EA00_OUT    = P1 ^ 4;
sbit DS28EA00_OUT_EN = P1 ^ 5;
sbit DS28EA00_IN     = P1 ^ 6;

// ROM address list
static unsigned char rom[DS28EA00_MAX_N_DEVICES][8];
static unsigned char n_dev = 0;

// Temperature array
static float temp[DS28EA00_MAX_N_DEVICES];

/*------------------------------------------------------------------*/

int DS28EA00_reset()
{
   int i;

   // set for output
   DS28EA00_OUT_EN = 0;

   // reset pulse 500 us
   DS28EA00_OUT = 0;
   delay_us(500); // tRSTL
   DS28EA00_OUT = 1;

   // set for input
   DS28EA00_OUT_EN = 1;

   // wait for presence pulse
   DELAY_US(75); // tMSP

   // chip present if line low
   i = (DS28EA00_IN == 0);

   DELAY_US(500); // tRSTH

   // set high
   DS28EA00_OUT = 1;

   // set for output
   DS28EA00_OUT_EN = 0;

   return i;
}

/*------------------------------------------------------------------*/

void DS28EA00_write_bit(unsigned char b)
{
   // set high
   DS28EA00_OUT = 1;

   // set for output
   DS28EA00_OUT_EN = 0;

   // set low
   DS28EA00_OUT = 0;

   if (b) {
      DELAY_US(15);   // tW1L
   } else {
      DELAY_US(105);  // tW0L
   }
   
   // set high
   DS28EA00_OUT = 1;

   if (b) {
      DELAY_US(105);  // tSLOT(120us) - tW1L
   } else {
      DELAY_US(15);   // tSLOT(120us) - tW0L
   }
}

/*------------------------------------------------------------------*/

void DS28EA00_write_byte(unsigned char byte)
{
   int i;

   for (i=0 ; i<8 ; i++)
      DS28EA00_write_bit((byte & (1<<i)) > 0);
}

/*------------------------------------------------------------------*/

int DS28EA00_read_bit()
{
   int b;

   // set high
   DS28EA00_OUT = 1;

   // set for output
   DS28EA00_OUT_EN = 0;

   // set low
   DS28EA00_OUT = 0;

   DELAY_US(10); // tRL

   // set for input
   DS28EA00_OUT_EN = 1;

   DELAY_US(20); // tMSR - tRL, 10 is too short

   b = DS28EA00_IN;

   DELAY_US(80); // tSLOT - tMSR

   return b;
}

/*------------------------------------------------------------------*/

void DS28EA00_low_impedance_on()
{
   // set high
   DS28EA00_OUT = 1;

   // set for output
   DS28EA00_OUT_EN = 0;

}

/*------------------------------------------------------------------*/

unsigned char DS28EA00_read_byte()
{
   unsigned char i, d;

   for (i=d=0 ; i<8 ; i++)
      d = d | (DS28EA00_read_bit() ? (1<<i) : 0);

   return d;
}

/*------------------------------------------------------------------*/


unsigned char DS28EA00_scan()
{
   int i, d;
 
   DS28EA00_reset();
   DS28EA00_write_byte(CMD_SKIP_ROM);
   DS28EA00_write_byte(CMD_CHAIN);
   DS28EA00_write_byte(CMD_CHAIN_ON);
   DS28EA00_write_byte(CMD_CHAIN_ON_INV);
   DS28EA00_low_impedance_on();
   d = DS28EA00_read_byte();
   delay_us(500); // wait for chain to charge

   for (n_dev=0 ; n_dev<DS28EA00_MAX_N_DEVICES ; n_dev++) {
      watchdog_refresh(0);
      DS28EA00_reset();
      DS28EA00_write_byte(CMD_COND_READ_ROM);
      for (i=0 ; i<8 ; i++)
         rom[n_dev][i] = DS28EA00_read_byte();

      if (rom[n_dev][0] == 0xFF) {
         break;
      }

      DS28EA00_write_byte(CMD_CHAIN);
      DS28EA00_write_byte(CMD_CHAIN_DONE);
      DS28EA00_write_byte(CMD_CHAIN_DONE_INV);

      d = DS28EA00_read_byte();
   }

   delay_ms(10);
   DS28EA00_reset();
   DS28EA00_write_byte(CMD_SKIP_ROM);
   DS28EA00_write_byte(CMD_CHAIN);
   DS28EA00_write_byte(CMD_CHAIN_OFF);
   DS28EA00_write_byte(CMD_CHAIN_OFF_INV);
   DS28EA00_low_impedance_on();
   d = DS28EA00_read_byte();

   return n_dev;
}


/*------------------------------------------------------------------*/

float DS28EA00_read(unsigned char index)
{
   static unsigned long last_convert = 0;

   int i, d, dev;
   unsigned char sp[10];
   
   if (time() > last_convert + 75) {

      // read each device sequentially
      for (dev=0 ; dev<n_dev ; dev++) {
         watchdog_refresh(0);
         d = DS28EA00_reset();
         if (!d) {
            // no devices on bus -> reset device list
            memset(rom, 0xFF, sizeof(rom));
            n_dev = 0;
            return 0;
         }
         DS28EA00_write_byte(CMD_MATCH_ROM);
         for (i=0 ; i<8 ; i++)
            DS28EA00_write_byte(rom[dev][i]);
         DS28EA00_write_byte(CMD_READ_SCRATCHPAD);
         sp[0] = DS28EA00_read_byte();
         sp[1] = DS28EA00_read_byte();
         temp[dev] = (float) ((short)(sp[0] | (sp[1] << 8))) * 85 / 0x550;
      }

      // start new conversion
      DS28EA00_reset();
      DS28EA00_write_byte(CMD_SKIP_ROM);
      DS28EA00_write_byte(CMD_CONVERT_TEMP);
      DS28EA00_low_impedance_on();

      last_convert = time();
   }

   return temp[index];
}
