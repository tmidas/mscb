/********************************************************************\

  Name:         i2c.h
  Created by:   Stefan Ritt

  Contents:     I2C interface header file

\********************************************************************/

void i2c_init();
void i2c_set_mux(unsigned char adr);
unsigned char i2c_get_mux();
void i2c_write_byte(unsigned char adr, unsigned char reg, unsigned char d);
void i2c_write_register(unsigned char adr, unsigned char reg);
void i2c_write_word(unsigned char adr, unsigned char reg, unsigned short d);
unsigned char i2c_read(unsigned char ack);
unsigned char i2c_read_byte(unsigned char adr, unsigned char reg);
unsigned short i2c_read_word(unsigned char adr, unsigned char reg);
unsigned long i2c_read_doubleword(unsigned char adr, unsigned char reg);

