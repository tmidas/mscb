/********************************************************************\

  Name:         pdcc.c
  Created by:   Stefan Ritt


  Contents:     Application specific (user) part of
                Midas Slow Control Bus protocol 
                for Mu3e Power DC/DC Controller

\********************************************************************/

#include <stdio.h>
#include <stdlib.h>             // for atof()
#include <string.h>
#include <math.h>
#include <intrins.h>
#include "mscbemb.h"
#include "gpio.h"
#include "spi.h"
#include "i2c.h"
#include "DS28EA00.h"
#include "pdcc.h"

extern bit FREEZE_MODE;
extern bit DEBUG_MODE;

char code node_name[] = "PDCC";

/* declare number of sub-addresses to framework */
unsigned char idata _n_sub_addr = 1;

bit flush_flag;

sbit INTERLOCK_OUT = P2^2;

unsigned short return_code;

/*---- Define variable parameters returned to CMD_GET_INFO command ----*/

/* data buffer (mirrored in EEPROM) */

struct {
   unsigned char en;
   unsigned char reset;
   unsigned char clear_fault;
    
   unsigned char on[16];

   unsigned short enable;
   unsigned short alert;
   
   unsigned short present;
   unsigned short power_good;
   unsigned short over_temp;
    
   unsigned short inv_underv;
   unsigned short ini_overi;
   unsigned short outv_overv;
   unsigned short outp_onoff;
   unsigned short cml_fault;
    
   unsigned short interlock;
    
   unsigned short debug1;
   unsigned short debug2;
   unsigned short debug3;
   
   unsigned char  n_temp;
   
   float v_adjust[16];
   
   float v_diode[16];
   float v_threshold[16];
   float v_mupix[16];
   float current[16];
   float temp[16];
   
} xdata user_data;


MSCB_INFO_VAR code vars[] = {
    
    1, UNIT_BOOLEAN, 0, 0,            0,                "CrateEn",  &user_data.en,              // array index: 0
                                                     
    //separate bool's for the DC-DC converters       
    1, UNIT_BOOLEAN, 0, 0,            0,                "Ch1En",    &user_data.on[0],           // 1
    1, UNIT_BOOLEAN, 0, 0,            0,                "Ch2En",    &user_data.on[1],           // 2
    1, UNIT_BOOLEAN, 0, 0,            0,                "Ch3En",    &user_data.on[2],           // 3
    1, UNIT_BOOLEAN, 0, 0,            0,                "Ch4En",    &user_data.on[3],           // 4
    1, UNIT_BOOLEAN, 0, 0,            0,                "Ch5En",    &user_data.on[4],           // 5
    1, UNIT_BOOLEAN, 0, 0,            0,                "Ch6En",    &user_data.on[5],           // 6
    1, UNIT_BOOLEAN, 0, 0,            0,                "Ch7En",    &user_data.on[6],           // 7
    1, UNIT_BOOLEAN, 0, 0,            0,                "Ch8En",    &user_data.on[7],           // 8
    1, UNIT_BOOLEAN, 0, 0,            0,                "Ch9En",    &user_data.on[8],           // 9
    1, UNIT_BOOLEAN, 0, 0,            0,                "Ch10En",   &user_data.on[9],           // 10
    1, UNIT_BOOLEAN, 0, 0,            0,                "Ch11En",   &user_data.on[10],          // 11
    1, UNIT_BOOLEAN, 0, 0,            0,                "Ch12En",   &user_data.on[11],          // 12
    1, UNIT_BOOLEAN, 0, 0,            0,                "Ch13En",   &user_data.on[12],          // 13
    1, UNIT_BOOLEAN, 0, 0,            0,                "Ch14En",   &user_data.on[13],          // 14
    1, UNIT_BOOLEAN, 0, 0,            0,                "Ch15En",   &user_data.on[14],          // 15
    1, UNIT_BOOLEAN, 0, 0,            0,                "Ch16OEn",  &user_data.on[15],          // 16
                                                        
    1, UNIT_BOOLEAN, 0, 0,            0,                "Reset",    &user_data.reset,           // 17
    1, UNIT_BOOLEAN, 0, 0,            0,                "ClrFlt",   &user_data.clear_fault,     // 18    
    2, UNIT_WORD,    0, 0,            0,                "Alert",    &user_data.alert,           // 19                                                                                              
    2, UNIT_WORD,    0, 0,            0,                "Present",  &user_data.present,         // 20
    2, UNIT_WORD,    0, 0,            0,                "PwrGood",  &user_data.power_good,      // 21
                                                    
    // selected bits from the TPS STATUS_WORD        
    2, UNIT_WORD,    0, 0,            0,                "OvrTmp",   &user_data.over_temp,       // 22
    2, UNIT_WORD,    0, 0,            0,                "VIN_UV",   &user_data.inv_underv,      // 23
    2, UNIT_WORD,    0, 0,            0,                "IOUT_OC",  &user_data.ini_overi,       // 24
    2, UNIT_WORD,    0, 0,            0,                "VOUT_OV",  &user_data.outv_overv,      // 25
    2, UNIT_WORD,    0, 0,            0,                "ONOFF",    &user_data.outp_onoff,      // 26  
                                                                                              
    2, UNIT_WORD,    0, 0,            0,                "Intlk",    &user_data.interlock,       // 27                                                                                              
    1, UNIT_BYTE,    0, 0,            0,                "NTemp",    &user_data.n_temp,          // 28
                                                      
    4, UNIT_PERCENT,  0, 0, MSCBF_FLOAT,                "Vadj1",    &user_data.v_adjust[0],     // 29
    4, UNIT_PERCENT,  0, 0, MSCBF_FLOAT,                "Vadj2",    &user_data.v_adjust[1],     // 30
    4, UNIT_PERCENT,  0, 0, MSCBF_FLOAT,                "Vadj3",    &user_data.v_adjust[2],     // 31
    4, UNIT_PERCENT,  0, 0, MSCBF_FLOAT,                "Vadj4",    &user_data.v_adjust[3],     // 32
    4, UNIT_PERCENT,  0, 0, MSCBF_FLOAT,                "Vadj5",    &user_data.v_adjust[4],     // 33
    4, UNIT_PERCENT,  0, 0, MSCBF_FLOAT,                "Vadj6",    &user_data.v_adjust[5],     // 34
    4, UNIT_PERCENT,  0, 0, MSCBF_FLOAT,                "Vadj7",    &user_data.v_adjust[6],     // 35
    4, UNIT_PERCENT,  0, 0, MSCBF_FLOAT,                "Vadj8",    &user_data.v_adjust[7],     // 36
    4, UNIT_PERCENT,  0, 0, MSCBF_FLOAT,                "Vadj9",    &user_data.v_adjust[8],     // 37
    4, UNIT_PERCENT,  0, 0, MSCBF_FLOAT,                "Vadj10",   &user_data.v_adjust[9],     // 38
    4, UNIT_PERCENT,  0, 0, MSCBF_FLOAT,                "Vadj11",   &user_data.v_adjust[10],    // 39
    4, UNIT_PERCENT,  0, 0, MSCBF_FLOAT,                "Vadj12",   &user_data.v_adjust[11],    // 40
    4, UNIT_PERCENT,  0, 0, MSCBF_FLOAT,                "Vadj13",   &user_data.v_adjust[12],    // 41
    4, UNIT_PERCENT,  0, 0, MSCBF_FLOAT,                "Vadj14",   &user_data.v_adjust[13],    // 42
    4, UNIT_PERCENT,  0, 0, MSCBF_FLOAT,                "Vadj15",   &user_data.v_adjust[14],    // 43
    4, UNIT_PERCENT,  0, 0, MSCBF_FLOAT,                "Vadj16",   &user_data.v_adjust[15],    // 44
                                                                                               
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "Vd1",      &user_data.v_diode[0],      // 45
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "Vd2",      &user_data.v_diode[1],      // 46
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "Vd3",      &user_data.v_diode[2],      // 47
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "Vd4",      &user_data.v_diode[3],      // 48
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "Vd5",      &user_data.v_diode[4],      // 49
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "Vd6",      &user_data.v_diode[5],      // 50
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "Vd7",      &user_data.v_diode[6],      // 51
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "Vd8",      &user_data.v_diode[7],      // 52
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "Vd9",      &user_data.v_diode[8],      // 53
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "Vd10",     &user_data.v_diode[9],      // 54
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "Vd11",     &user_data.v_diode[10],     // 55
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "Vd12",     &user_data.v_diode[11],     // 56
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "Vd13",     &user_data.v_diode[12],     // 57
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "Vd14",     &user_data.v_diode[13],     // 58
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "Vd15",     &user_data.v_diode[14],     // 59
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "Vd16",     &user_data.v_diode[15],     // 60
                                                     
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "Vthr1",    &user_data.v_threshold[0],  // 61
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "Vthr2",    &user_data.v_threshold[1],  // 62
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "Vthr3",    &user_data.v_threshold[2],  // 63
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "Vthr4",    &user_data.v_threshold[3],  // 64
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "Vthr5",    &user_data.v_threshold[4],  // 65
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "Vthr6",    &user_data.v_threshold[5],  // 66
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "Vthr7",    &user_data.v_threshold[6],  // 67
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "Vthr8",    &user_data.v_threshold[7],  // 68
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "Vthr9",    &user_data.v_threshold[8],  // 69
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "Vthr10",   &user_data.v_threshold[9],  // 70
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "Vthr11",   &user_data.v_threshold[10], // 71
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "Vthr12",   &user_data.v_threshold[11], // 72
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "Vthr13",   &user_data.v_threshold[12], // 73
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "Vthr14",   &user_data.v_threshold[13], // 74
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "Vthr15",   &user_data.v_threshold[14], // 75
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "Vthr16",   &user_data.v_threshold[15], // 76
                                                     
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "VmuPix1",  &user_data.v_mupix[0],      // 77
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "VmuPix2",  &user_data.v_mupix[1],      // 78
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "VmuPix3",  &user_data.v_mupix[2],      // 79
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "VmuPix4",  &user_data.v_mupix[3],      // 80
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "VmuPix5",  &user_data.v_mupix[4],      // 81
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "VmuPix6",  &user_data.v_mupix[5],      // 82
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "VmuPix7",  &user_data.v_mupix[6],      // 83
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "VmuPix8",  &user_data.v_mupix[7],      // 84
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "VmuPix9",  &user_data.v_mupix[8],      // 85
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "VmuPix10", &user_data.v_mupix[9],      // 86
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "VmuPix11", &user_data.v_mupix[10],     // 87
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "VmuPix12", &user_data.v_mupix[11],     // 88
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "VmuPix13", &user_data.v_mupix[12],     // 89
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "VmuPix14", &user_data.v_mupix[13],     // 90
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "VmuPix15", &user_data.v_mupix[14],     // 91
    4, UNIT_VOLT,     0, 0, MSCBF_FLOAT ,               "VmuPix16", &user_data.v_mupix[15],     // 92

    4, UNIT_AMPERE,   0, 0, MSCBF_FLOAT ,               "I1",       &user_data.current[0],      // 93
    4, UNIT_AMPERE,   0, 0, MSCBF_FLOAT ,               "I2",       &user_data.current[1],      // 94
    4, UNIT_AMPERE,   0, 0, MSCBF_FLOAT ,               "I3",       &user_data.current[2],      // 95
    4, UNIT_AMPERE,   0, 0, MSCBF_FLOAT ,               "I4",       &user_data.current[3],      // 96
    4, UNIT_AMPERE,   0, 0, MSCBF_FLOAT ,               "I5",       &user_data.current[4],      // 97
    4, UNIT_AMPERE,   0, 0, MSCBF_FLOAT ,               "I6",       &user_data.current[5],      // 98
    4, UNIT_AMPERE,   0, 0, MSCBF_FLOAT ,               "I7",       &user_data.current[6],      // 99
    4, UNIT_AMPERE,   0, 0, MSCBF_FLOAT ,               "I8",       &user_data.current[7],      // 100
    4, UNIT_AMPERE,   0, 0, MSCBF_FLOAT ,               "I9",       &user_data.current[8],      // 101
    4, UNIT_AMPERE,   0, 0, MSCBF_FLOAT ,               "I10",      &user_data.current[9],      // 102
    4, UNIT_AMPERE,   0, 0, MSCBF_FLOAT ,               "I11",      &user_data.current[10],     // 103
    4, UNIT_AMPERE,   0, 0, MSCBF_FLOAT ,               "I12",      &user_data.current[11],     // 104
    4, UNIT_AMPERE,   0, 0, MSCBF_FLOAT ,               "I13",      &user_data.current[12],     // 105
    4, UNIT_AMPERE,   0, 0, MSCBF_FLOAT ,               "I14",      &user_data.current[13],     // 106
    4, UNIT_AMPERE,   0, 0, MSCBF_FLOAT ,               "I15",      &user_data.current[14],     // 107
    4, UNIT_AMPERE,   0, 0, MSCBF_FLOAT ,               "I16",      &user_data.current[15],     // 108

    4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT  ,              "Tmp1",     &user_data.temp[0],         // 109
    4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "Tmp2",     &user_data.temp[1],         // 110
    4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "Tmp3",     &user_data.temp[2],         // 111
    4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "Tmp4",     &user_data.temp[3],         // 112
    4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "Tmp5",     &user_data.temp[4],         // 113
    4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "Tmp6",     &user_data.temp[5],         // 114
    4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "Tmp7",     &user_data.temp[6],         // 115
    4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "Tmp8",     &user_data.temp[7],         // 116
    4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "Tmp9",     &user_data.temp[8],         // 117
    4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "Tmp10",    &user_data.temp[9],         // 118
    4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "Tmp11",    &user_data.temp[10],        // 119
    4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "Tmp12",    &user_data.temp[11],        // 120
    4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "Tmp13",    &user_data.temp[12],        // 121
    4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "Tmp14",    &user_data.temp[13],        // 122
    4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "Tmp15",    &user_data.temp[14],        // 123
    4, UNIT_CELSIUS,  0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "Tmp16",    &user_data.temp[15],        // 124
    
    2, UNIT_WORD,     0, 0,                           0, "CML",     &user_data.cml_fault,       // 125
                                                      
    2, UNIT_WORD,     0, 0,                           0, "Debug1",  &user_data.debug1,          // 126
    2, UNIT_WORD,     0, 0,                           0, "Debug2",  &user_data.debug2,          // 127
    2, UNIT_WORD,     0, 0,                           0, "Debug3",  &user_data.debug3,          // 128
0                                                     

};

MSCB_INFO_VAR *variables = vars;

unsigned char xdata update_data[sizeof(vars) / sizeof(MSCB_INFO_VAR)];

/********************************************************************\

  Application specific init and inout/output routines

\********************************************************************/

void user_write(unsigned char index) reentrant;

/*---- User init function ------------------------------------------*/

extern SYS_INFO idata sys_info;

void user_init(unsigned char init)
{
    unsigned char i;
    if (init);
    SFRPAGE = LEGACY_PAGE;
    
    //  output to 1, inputs to 0
    // see https://mu3epartsdb.physi.uni-heidelberg.de/showrecord/0560 for uC pins
    
    //P0MDOUT = 0x5D; // version A
    P0MDOUT = 0xDD; // version B
   // P1MDOUT = 0xFF; // version A, has to be 0xB7, to be tested
    P1MDOUT = 0xB5; // version B
    P2MDOUT = 0xFF;
   
    spi_init();
    gpio_init();
    i2c_init();
   
    // avoid linker error by calling routines once
    i2c_set_mux(-1);
    i2c_get_mux();
    i2c_write_byte(0,0,0);
    i2c_read_byte(0,0);
    i2c_read_word(0,0);
    slot_to_pin(0);
    led_rw_blink_get();
    led_rw_blink_set(1);
    DS28EA00_read(0);
    read_adc_temp(-1);
    read_adc(-1);
    read_adc_filtered(-1);

    
    
    
    // initialize temperatures 
    for (i=0 ; i<16 ; i++)
        user_data.temp[i] = -999;
        
    // initialize  DC-DC monitoring data
    for(i=0 ; i<16 ; i++){
        user_data.v_adjust[i] = 0.0;
        user_data.v_diode[i] = 0.0;
        user_data.v_threshold[i]=0.0;
        user_data.v_mupix[i]=0.0;
        user_data.current[i]=0.0;
        user_data.on[i]=0;
        
    }
   
    user_data.debug1 = 0;
    user_data.debug2 = 0;
    user_data.debug3 = 0;
    user_data.over_temp = 0;
    user_data.clear_fault = 0;
    user_data.cml_fault = 0;
    user_data.cml_fault = 0;                
    user_data.over_temp = 0;
    user_data.inv_underv = 0;
    user_data.ini_overi = 0;
    user_data.outv_overv = 0;
    user_data.outp_onoff = 0xFF;
    
    return_code = 0;    
    
    // scan 1-wire bus
    user_data.n_temp = DS28EA00_scan();	

}

/*---- User write function -----------------------------------------*/

#pragma NOAREGS

void user_write(unsigned char index) reentrant
{
   update_data[index] = 1;
}

/*---- User read function ------------------------------------------*/

unsigned char user_read(unsigned char index)
{
   if (index == 0);
   return 0;
}

/*---- User function called vid CMD_USER command -------------------*/

unsigned char user_func(unsigned char *data_in, unsigned char *data_out)
{
   /* echo input data */
   data_out[0] = data_in[0];
   data_out[1] = data_in[1];

   return 2;
}




/*---- Utility functions for the DC-DC crate and board ----*/

unsigned char slot_to_pin(unsigned char slot){   // mapping the 1-16 slots of the dc-dc backplane to the pin sequence on the controller, zero counted
    
    
    switch(slot){
        case 15:
            return 11;
        case 14:
            return 10;
        case 0:
            return 0;
        case 1:
            return 1;
        case 13:
            return 9;
        case 12:
            return 8;
        case 2:
            return 2;
        case 11:
            return 15;
        case 3:
            return 3;
        case 10:
            return 14;
        case 4:
            return 4;
        case 5:
            return 5;
        case 6:
            return 6;
        case 7:
            return 7; 
        case 8:
            return 12;
        case 9:
            return 13;

    }
    return 0;
}




// Detector the slots where we have DC-DC converters
unsigned short check_presence()
{
    unsigned char i_slot;
    unsigned int list = 0x0000;
    unsigned char reply;
    
    for(i_slot=0;i_slot<N_SLOTS;i_slot++)
    {
        i2c_set_mux(slot_to_pin(i_slot));
        i2c_write_byte(TPS_ADDR, TPS_FREQUENCY_CONFIG_REG, TPS_FREQUENCY_1MHZ);        
    }
    
    DELAY_US(1000);
    
    for(i_slot=0;i_slot<N_SLOTS;i_slot++)
    {
        i2c_set_mux(slot_to_pin(i_slot));
        reply = i2c_read_byte(TPS_ADDR, TPS_FREQUENCY_CONFIG_REG);
        if(reply == TPS_FREQUENCY_1MHZ)
        {
            list = list | 0x01 << i_slot;
        }      
        
    }
    
    //list = list | 0x01 << 2;
    
    return list;
}


unsigned char config_AD5933_Sense()
{
    return 0;
}


unsigned char config_AD5933()
{
    //AD5593R Software Reset
    //i2c_write_word(AD5593_ADDR, 0x0F, 0x5AC);
    
    unsigned short gpio_in_pins = 0x0000;
    unsigned short gpio_out_pins = 0x0000;    
    unsigned short adc_in_pins = 0x0000;
    
    
    		
    i2c_write_word(AD5593_ADDR, AD5593_PD_REF_CTRL, 0x0200); //set Vref of ADC/GPIO as internal
    DELAY_US(10);
    i2c_write_word(AD5593_ADDR, AD5593_GEN_CTRL_REG, 0x0020); //
    DELAY_US(10);
    
    //
    // GPIO pin config
    //
    
    gpio_in_pins = gpio_in_pins | PGOOD_PIN; 
    gpio_in_pins = gpio_in_pins | INTERLOCK_COMP_PIN; 
    
    gpio_out_pins = gpio_out_pins | LED_PIN;
    gpio_out_pins = gpio_out_pins | EN_PIN;        
                
    //configure the ADC/GPIO pins
    i2c_write_word(AD5593_ADDR, AD5593_GPIO_OPENDRAIN_REG, gpio_out_pins); //sets pin as push/pull        
 	DELAY_US(10);
        
    // enable GPIO pin for LED & Enable
    i2c_write_word(AD5593_ADDR, AD5593_GPIO_INPUT_REG, gpio_in_pins); //sets pin  as genral purpose input
    DELAY_US(10);
	i2c_write_word(AD5593_ADDR, AD5593_GPIO_CONFIG_REG, gpio_out_pins);
    DELAY_US(10);
    i2c_write_word(AD5593_ADDR, AD5593_GPIO_OUTPUT_REG, 0x00); // make sute the enable out is low    
   
    //
    // ADC pin config
    //

    adc_in_pins = adc_in_pins | VDIODE_PIN; 
    adc_in_pins = adc_in_pins | VTHRESH_PIN; 
    adc_in_pins = adc_in_pins | VMUPIX_PIN; 
    adc_in_pins = adc_in_pins | CURRENT_PIN; 
    
    
    i2c_write_word(AD5593_ADDR,AD5593_ADC_CONFIG_REG,adc_in_pins);   
       
    return 0;
}


unsigned char config_TPS53819A()
{
    unsigned char on_off_config = 0xFF;
    unsigned char soft_start_config = 0x00;
    unsigned char operation_config = 0x00;
    
    i2c_write_byte(TPS_ADDR, TPS_FREQUENCY_CONFIG_REG, TPS_FREQUENCY_1MHZ);
    DELAY_US(100);
    
    on_off_config = on_off_config & ~(0x01 << 3); //ignore on_off bit for operation, only listen to enable pin
    i2c_write_byte(TPS_ADDR,TPS_ON_OFF_CONFIG_REG,on_off_config);
    DELAY_US(10)
    
    soft_start_config = soft_start_config | 0x01 ; //set FCCM mode
    soft_start_config = soft_start_config | 0x02 ; //turn of hiccup after UV
    soft_start_config = soft_start_config | 0x04 ; //set soft start timing. 0x00 = 1 ms, 0x04 = 2 ms, 0x08 = 4 ms, 0x0C = 8 ms; 
    
    i2c_write_byte(TPS_ADDR,TPS_MODE_SOFT_START_CONFIG_REG,soft_start_config);
    
    i2c_write_byte(TPS_ADDR,TPS_UVLO_THRESHOLD_REG,0x05); //under voltage at the input, 0x00 = 10.2V, 0x05 = 4.25V, 0x06 = 6V, 0x07 = 8.1V
    
    i2c_write_byte(TPS_ADDR,TPS_VOUT_ADJUSTMENT_REG,0x10); //0x10 = 0%(default)
    
    
    //operation_config = operation_config | 0xA8  ;  //turn on VOMH and act on faults
    //operation_config = operatiopn_config | 0x24  ;  //turn on VOMH and ignore faults
    //operation_config = operatiopn_config | 0x18  ;  //turn on VOML and act on faults
    //operation_config = operatiopn_config | 0x14  ;  //turn on VOML and ignore faults
    
    i2c_write_byte(TPS_ADDR,TPS_VOUT_MARGIN_REG,0x00);
    DELAY_US(10);
    i2c_write_byte(TPS_ADDR,TPS_OPERATION_REG,0xA8); //0x28 does not work, so it want the on bit for margining, even if we have desabled acting on that bit???
                                                     // be carefull, as the DC-DC converter is not switched on just because ignore the on-off bit. but we set it here to 1
    DELAY_US(100);
    
    return 0;
}

// expects a value between -9 and +9,
// sure I can do some smart parsing, but I prefer the verbatum lookup table from the datasheet
//rounded to the smaller adjustment
unsigned char get_vout_adj_code(float value)
{
    if( value > 8.75 ) return 0x1F; // 0b00011111
    if( value > 8.00 ) return 0x1B; // 0b00011011
    if( value > 7.25 ) return 0x1A; // 0b00011010
    if( value > 6.50 ) return 0x19; // 0b00011001
    if( value > 5.75 ) return 0x18; // 0b00011000
    if( value > 5.00 ) return 0x17; // 0b00010111
    if( value > 4.25 ) return 0x16; // 0b00010110
    if( value > 3.50 ) return 0x15; // 0b00010101
    if( value > 2.75 ) return 0x14; // 0b00010100
    if( value > 2.00 ) return 0x13; // 0b00010011
    if( value > 1.25 ) return 0x12; // 0b00010010
    if( value > 0.50 ) return 0x11; // 0b00010001    
    if( value > -0.50 ) return 0x10; // 0b00010000 // TI default for 0% trim
    if( value > -1.25 ) return 0x0E; // 0b00001110
    if( value > -2.00 ) return 0x0D; // 0b00001101
    if( value > -2.75 ) return 0x0C; // 0b00001100
    if( value > -3.50 ) return 0x0B; // 0b00001011
    if( value > -4.25 ) return 0x0A; // 0b00001010
    if( value > -5.00 ) return 0x09; // 0b00001001
    if( value > -5.75 ) return 0x08; // 0b00001000
    if( value > -6.50 ) return 0x07; // 0b00000111
    if( value > -7.25 ) return 0x06; // 0b00000110
    if( value > -8.00 ) return 0x05; // 0b00000101
    if( value > -8.75 ) return 0x04; // 0b00000100
    return 0x00; // -9%    
}

// expects a value between 0 and 12.0
unsigned char get_vout_margin_code(float value)
{
    if( value > 11.9 ) return 0xF0; // 0b11110000
    if( value > 10.8 ) return 0xB0; // 0b10110000
    if( value > 9.8 ) return 0xA0;  // 0b10100000
    if( value > 8.7 ) return 0x90;  // 0b10010000
    if( value > 7.6 ) return 0x80;  // 0b10000000
    if( value > 6.6 ) return 0x70;  // 0b01110000
    if( value > 5.6 ) return 0x60;  // 0b01100000
    if( value > 4.6 ) return 0x50;  // 0b01010000
    if( value > 3.6 ) return 0x40;  // 0b01000000
    if( value > 2.7 ) return 0x30;  // 0b00110000
    if( value > 1.7 ) return 0x20;  // 0b00100000
    if( value > 0.8 ) return 0x10;  // 0b00010000
    return 0x00; // 0%
}


// expects a number between -9 and +22.08   
unsigned char trim_vout(float trim_percentage)
{
    float vout_adj, vout_margin;
    unsigned char vout_adj_code,vout_margin_code;
    if( trim_percentage < -9.1 || trim_percentage > 22.1) return 1;
    
    //good enough for VOUT_ADJUSTMENT alone
    if( trim_percentage >= -9 && trim_percentage < 9){
        
        i2c_write_byte(TPS_ADDR,TPS_VOUT_MARGIN_REG,0x00);
        DELAY_US(10);
        //i2c_write_byte(TPS_ADDR,TPS_OPERATION_REG,0xA8); 
        DELAY_US(10);
        
        
        vout_adj = trim_percentage;
        vout_adj_code = get_vout_adj_code(vout_adj);
        i2c_write_byte(TPS_ADDR,TPS_VOUT_ADJUSTMENT_REG,vout_adj_code);
    } else {
        vout_adj = 9.0;
        vout_adj_code = 0x1F;
        
        
        i2c_write_byte(TPS_ADDR,TPS_VOUT_ADJUSTMENT_REG,vout_adj_code);
        
        DELAY_US(10);
   
        vout_margin = ((1+trim_percentage/100.)/1.09)-1;
        vout_margin_code = get_vout_margin_code(100.*vout_margin);               
        
        i2c_write_byte(TPS_ADDR,TPS_VOUT_MARGIN_REG,vout_margin_code);
        user_data.debug2 = vout_margin_code;
        DELAY_US(10);
        //i2c_write_byte(TPS_ADDR,TPS_OPERATION_REG,0xA8); //0x28 does not work, so it want the on bit for margining, even if we have desabled acting on that bit???
        DELAY_US(10);
    }
    
        
    return 0;
}

// initiate a adc read for a single pin + temperature measurement
float read_adc_temp(unsigned short pin){
    unsigned long reply;
    float full_value, temperature;    
    i2c_write_word(AD5593_ADDR,AD5593_ADC_SEQ_REG, pin | 0x0100 );
    DELAY_US(100);
    reply = i2c_read_doubleword(AD5593_ADDR,AD5593_ADC_RD_POINTER);
    i2c_write_word(AD5593_ADDR,AD5593_ADC_SEQ_REG,  0x0000 );
    full_value = 2 * ( 0x00000FFF & reply ) * 2.5/4096 ;  // the first 2* is because the adc gain is set to 2x. if we change that, also change here.
    
    temperature = 25 + ( ((reply >> 16) & 0x0FFF) - (0.5/2*5)*4095 ) / ( 1.327 * (2.5/5));
    user_data.temp[0] = temperature;
    //user_data.debug3 = ((reply >> 16) & 0x0FFF);
    //user_data.debug2 = ((reply ) & 0x0FFF);
    
    //calculate temperature
    
    
    return floor(full_value * 1000 + 0.5) / 1000;
}

// initiate a adc read for a single pin
float read_adc(unsigned short pin){
    unsigned short reply;
    float full_value;    
    i2c_write_word(AD5593_ADDR,AD5593_ADC_SEQ_REG, pin | 0x0200 );
    DELAY_US(1000);
    reply = i2c_read_word(AD5593_ADDR,AD5593_ADC_RD_POINTER);
    DELAY_US(10);
    i2c_write_word(AD5593_ADDR,AD5593_ADC_SEQ_REG,  0x0000 );
    DELAY_US(10);
    if(pin == CURRENT_PIN ) user_data.debug2 = 0x0FFF & reply;
    if(pin == VDIODE_PIN ) user_data.debug3 = 0x0FFF & reply;
    full_value = 2 * ( 0x0FFF & reply ) * 2.5/4096 ;  // the first 2* is because the adc gain is set to 2x. if we change that, also change here.
    return floor(full_value * 1000 + 0.5) / 1000;
}

float read_adc_filtered(unsigned short pin) {
    unsigned short reply;
    float values[NUM_READINGS];
    float full_value;
    int i,j;
    float temp, sum;

    // Take multiple readings
    for(i = 0; i < NUM_READINGS; i++) {
        i2c_write_word(AD5593_ADDR, AD5593_ADC_SEQ_REG, pin | 0x0000);
        DELAY_US(20); // min 2 us to allow for adc conversion time
        reply = i2c_read_word(AD5593_ADDR, AD5593_ADC_RD_POINTER);
        DELAY_US(20);
        i2c_write_word(AD5593_ADDR, AD5593_ADC_SEQ_REG, 0x0000);
        values[i] = 2 * (0x0FFF & reply) * 2.5 / 4096;  // Apply gain correction
        DELAY_US(20);
    }

    // Sort the values (simple bubble sort for small array)
    for(i = 0; i < NUM_READINGS - 1; i++) {
        for (j = 0; j < NUM_READINGS - i - 1; j++) {
            if (values[j] > values[j + 1]) {
                temp = values[j];
                values[j] = values[j + 1];
                values[j + 1] = temp;
            }
        }
    }

    // Compute the average of the middle values (discarding extremes)
    sum = 0;
    for (i = 2; i < NUM_READINGS - 2; i++) {
        sum += values[i];
    }
    full_value = sum / (NUM_READINGS - 4);  // Average middle values

    return floor(full_value * 1000 + 0.5) / 1000;  // Round to 3 decimal places
}

    
unsigned short get_bit(unsigned short word, unsigned char position){
    return ((word >> position) & 0x0001);
}

unsigned short set_bit(unsigned short word, unsigned char position, unsigned short value){
    return (word & ~(1 << position)) | ( value  << position );
}


unsigned char allowed_to_turn_on(unsigned char ch){
    
    return_code = 0;
    if( user_data.en != 1 ) return 0;
    return_code = 1;
    if( get_bit(user_data.interlock,ch) == 1 ) return 0;
    return_code = 2;
    if( get_bit(user_data.alert,ch) == 1) return 0; // alert has been pulled down and fault condition has to be addressed first
    return_code = 3;
    if( get_bit(user_data.over_temp,ch) == 1) return 0;
    return_code = 4;
    if( get_bit(user_data.ini_overi,ch) == 1) return 0;
    return_code = 5;
    if( get_bit(user_data.outv_overv,ch) == 1) return 0;
    return_code = 6;
    return 1;

}

/*---- User loop function ------------------------------------------*/

void user_loop(void)
{   
    static unsigned long last = 0;
    static unsigned long last1 = 0;
    static unsigned char bInit = 0;
    static unsigned char flag = 0;
    unsigned char i;
    static unsigned short presence_list = 0x0000;
    static unsigned short debug1 = 0x0000;
    static unsigned short debug2 = 0x0000;
    static unsigned short debug3 = 0x0000;
//  static unsigned short test  = 0x0020;
    
    static unsigned short out_pins = 0x0000;
    static unsigned short reply_word = 0x0000;
    unsigned short interlock_status = 0;    
    
    
   
    watchdog_refresh(0);
    
    
    // *******************************
    // init and reset functions
    // ********************************
    
    // We make sure all DC-DC converters are off on reboot. 100 ms delay after boot
    
    if (time() > 100 & !bInit) {
        
        bInit = 1;        
        
        presence_list = check_presence();
        user_data.present = presence_list;
     
        
        DELAY_US(10); //not clear if we need the delays
        
        //global reset on the AD5933 reset line
        gpio_out(0x0000); //definitely needed!!!
        DELAY_US(1); //min 250 ns
        gpio_out(0xFFFF);
        DELAY_US(100);
        
        for(i=0 ; i<N_SLOTS; i++){
            
            if( (presence_list >> i) & 0x1 ){
                
                i2c_set_mux(slot_to_pin(i));
                DELAY_US(100);
                config_TPS53819A();
                trim_vout(user_data.v_adjust[i]); //set trim
                DELAY_US(100);
                config_AD5933(); // also sets the EN pin low
                DELAY_US(100);
            }
        }
    }
    
   
        
    //
    // "Reset"
    //
   
    if(update_data[17]) {
        
		update_data[17] = 0;
        led_blink(0, 5, 50);        
        
        if( user_data.reset == 1 ){ // Issue reset to all channels
            
            gpio_out(0x0);
            DELAY_US(1); //min 250 ns
            gpio_out(0xFFFF);
            //after the reset, reinitialize the AD5933 settings
            
            for(i=0 ; i<N_SLOTS; i++){
                if( (presence_list >> i) & 0x1 ){     
                   i2c_set_mux(slot_to_pin(i));
                   DELAY_US(100);
                   config_TPS53819A();
                   DELAY_US(100); 
                   config_AD5933();
                   DELAY_US(100);
                }
            } 
		}      
        
        // reset reset command 
        user_data.reset = 0;        
    }
    
    //
    // "clear faults"
    //
    
    if(update_data[18]) {        
	    update_data[18] = 0;
        led_blink(0, 5, 50);        
        
        if( user_data.clear_fault == 1 ){ // Issue clear fault to all channels
            
            for(i=0 ; i<N_SLOTS; i++){
                if( (presence_list >> i) & 0x1 ){     
                   i2c_set_mux(slot_to_pin(i));
                   DELAY_US(100);
                   i2c_write_register(TPS_ADDR,TPS_CLEAR_FAULTS_REG);
                }
            }
            user_data.interlock = 0;
        }
        user_data.clear_fault = 0;
    } 
    
    
   // Init Firmware for the Sense Wire Board
    
    for(i=0 ; i<N_SLOTS; i++){
        if(  (presence_list >> i) & 0x1  ) continue; // if there is DC-DC board, skip
        i=15;
        i2c_set_mux(slot_to_pin(i));
        config_AD5933_Sense();
    }
  
   
    
    // **************************************************
    // from this point on everything has to be initialzed
    // **************************************************       

    if(!bInit) return;
    
    //
    // global enable for all channels
    //
    
    if(update_data[0]) {           // global on / off
        
        update_data[0]=0;
        
       
        // global power off triggered, don't ask questions, just turn everything off
        if(user_data.en == 0) {
            for(i=0 ; i<N_SLOTS ; i++)
            {
                i2c_set_mux(slot_to_pin(i));                
                DELAY_US(10);
                i2c_write_word(AD5593_ADDR, AD5593_GPIO_OUTPUT_REG, 0x00);
                user_data.on[i]=0;
            }
        }
    
       //for now, disable to global turning on of channels
        
//       if(user_data.on == 1) { // global power on         
//           for (i=0 ; i<N_SLOTS ; i++) {
//               if( (presence_list >> i) & 0x1 ){
//                   // switch the enabled boards on, if they are present, and if 
//                   //if( (get_bit(user_data.enable,i) & 1 == 1) && ( get_bit(user_data.interlock,i) == 0 )) {
//                   if( get_bit(user_data.enable,i) & 1 == 1) {
//                       out_pins = i2c_read_word(AD5593_ADDR, AD5593_GPIO_OUTPUT_REG | AD5593_REG_RD_POINTER );
//                       out_pins = out_pins | EN_PIN; // set EN_PIN
//                       i2c_write_word(AD5593_ADDR, AD5593_GPIO_OUTPUT_REG, out_pins);
//                   }
//               }s 1s
//           }
    }
    
    
    
    //
    // channel by channel turn on/off, and Trim VOUT
    //
    
    for (i=0 ; i<N_SLOTS ; i++){
        
        if( !( (presence_list >> i) & 0x1 ) ) continue; //if  a board is not there, don't try 
        
        // enable section
        if(update_data[1+i]){
            
            update_data[1+i]=0;
            
            i2c_set_mux(slot_to_pin(i));
            DELAY_US(10);
            
            out_pins = i2c_read_word(AD5593_ADDR, AD5593_GPIO_OUTPUT_REG | AD5593_REG_RD_POINTER );
            
            if(user_data.on[i]){
                
                if( allowed_to_turn_on(i) == 1 ){

                    out_pins = out_pins | EN_PIN;
                    i2c_write_word(AD5593_ADDR, AD5593_GPIO_OUTPUT_REG, out_pins);
                    
                } else {
                   user_data.on[i] = 0;
                }
            
            } else {
                out_pins = set_bit(out_pins,EN_PIN,0);
                i2c_write_word(AD5593_ADDR, AD5593_GPIO_OUTPUT_REG, 0x00);
            }            
        }
        
        //trim section
        if(update_data[29+i])  {
            
            update_data[29+i] = 0;
            
            i2c_set_mux(slot_to_pin(i));
            DELAY_US(10);
            trim_vout(user_data.v_adjust[i]);           
        }
        
    }
   
    
    //
    // do periodic tasks each 100 ms
    // only things that need to be checked very often
    //
    
    if (time() > last + 10 && bInit == 1) {
    // led_blink(0, 1, 50);
    
         
        
        DELAY_US(10);
		
        //loop over the existing DC-DC boards
        
        for(i=0 ; i<N_SLOTS; i++){
            if( (presence_list >> i) & 0x1 ){
            
                i2c_set_mux(slot_to_pin(i));                
                DELAY_US(10);
                reply_word = i2c_read_word(AD5593_ADDR, AD5593_GPIO_INPUT_REG | AD5593_GPIO_RD_POINTER);
                user_data.power_good = set_bit( user_data.power_good, i, get_bit(reply_word,PGOOD_PIN_POS ) );
                
                interlock_status = get_bit(reply_word,INTERLOCK_COMP_PIN_POS);
                if(interlock_status == 0x0001){  //interlock has just triggered! 
                    user_data.interlock = user_data.interlock | ( interlock_status  << i ); //latch
                    i2c_write_word(AD5593_ADDR, AD5593_GPIO_OUTPUT_REG, 0x00); // turn the enable off                  
                }
            }
        }
        //need to read Interlock status at a higher frequency
        last = time();
    }
    
    
    
   
    // do periodic tasks once per second
    if (time() > last1 + 100 && bInit == 1) {
        
        led_blink(0, 2, 50);        
 
		//user_data.debug = 0x0F & i2c_read_byte(TPS_ADDR, 0xd3);
        
        // read "ALERT" lines via GPIO1
        reply_word = gpio_in(); // comes in as the controller numbering, not the crate numbering
        for(i=0 ; i<N_SLOTS; i++){
            user_data.alert = set_bit( user_data.alert , i, get_bit(reply_word,slot_to_pin(i)) );
        }
        
        
      		
		DELAY_US(10);
		
        //loop over the existing DC-DC boards
        
        for(i=0 ; i<N_SLOTS; i++){

            if( (presence_list >> i) & 0x1 ){
            
                i2c_set_mux(slot_to_pin(i));
                DELAY_US(10);
                
                // set LED if the board is present, blink if power good
                out_pins = i2c_read_word(AD5593_ADDR, AD5593_GPIO_OUTPUT_REG | AD5593_REG_RD_POINTER );
                if( get_bit(user_data.power_good,i) ){
                    out_pins = out_pins ^ LED_PIN; // toggle LED
                } else {
                    out_pins = out_pins | LED_PIN; // set LED
                }
                
                i2c_write_word(AD5593_ADDR, AD5593_GPIO_OUTPUT_REG, out_pins);
                
                // READ ADC values
                user_data.v_mupix[i] = read_adc_filtered(VMUPIX_PIN);
                user_data.v_threshold[i] = read_adc_filtered(VTHRESH_PIN);
                user_data.v_diode[i] = read_adc_filtered(VDIODE_PIN);
                DELAY_US(1000);
                user_data.current[i] = read_adc_filtered(CURRENT_PIN)/(20*0.002); //2 mOhm shunt resistor, gain of 20
                DELAY_US(10);
                
                reply_word = i2c_read_word(TPS_ADDR,TPS_STATUS_WORD_REG); // this way I get 0x_LOWBYTE_HIGHBYTE, see
                user_data.cml_fault = set_bit( user_data.cml_fault, i, get_bit(reply_word,TPS_CML_POS ) );                
                user_data.over_temp = set_bit( user_data.over_temp, i, get_bit(reply_word,TPS_TEMP_POS ) );
                user_data.inv_underv = set_bit( user_data.inv_underv, i, get_bit(reply_word,TPS_VIN_UV_POS ) );
                user_data.ini_overi = set_bit( user_data.ini_overi, i, get_bit(reply_word,TPS_IOUT_OC_POS ) );
                user_data.outv_overv = set_bit( user_data.outv_overv, i, get_bit(reply_word,TPS_VOUT_OV_POS ) );
                user_data.outp_onoff = set_bit( user_data.outp_onoff, i, get_bit(reply_word,TPS_OFF_POS ) );
              
            
                if(i==14){
                    debug1 = reply_word;
                    //c = return_code;
                    //debug3 = i2c_read_word(AD5593_ADDR, AD5593_GPIO_OUTPUT_REG | AD5593_REG_RD_POINTER );
                }
                //if(i==4) debug = i2c_read_word(AD5593_ADDR, 0x05 | 0x70);                     
            
            }
            
            
            
            
            //user_data.debug = i2c_read_word(AD5593_ADDR, AD5593_GPIO_CONFIG_REG);
        }
            
      
        flag = !flag;        

          
        // read 1-wire temperature sensor periodically
        //for (i=0 ; i<user_data.n_temp ; i++) 
        //    user_data.temp[i] = DS28EA00_read(i);
            
        last1 = time();
        
        user_data.debug1 = debug1;
        //user_data.debug2 = debug2;
        //user_data.debug3 = debug3;
   }
    
   
   // LOOP Firmware for the Sense Wire Board   
   

}
