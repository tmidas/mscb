/********************************************************************\

  Name:         gpio.h
  Created by:   Stefan Ritt

  Contents:     Header file  for MCP23S17 General Purpose IO Chip

\********************************************************************/
      
void gpio_init();
void gpio_write(unsigned char device, unsigned char adr, unsigned char d);
void gpio_out(unsigned short d);
unsigned char gpio_read(unsigned char device, unsigned char adr);
unsigned short gpio_in();
