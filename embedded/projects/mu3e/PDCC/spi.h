/********************************************************************\

  Name:         spi.h
  Created by:   Stefan Ritt

  Contents:     SPI interface header file

\********************************************************************/

void spi_init();
void spi_adr();
void spi_deadr();

void spi_write_lsb(unsigned char d);
void spi_write_msb(unsigned char d);
unsigned char spi_read_msb();
