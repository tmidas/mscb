/********************************************************************\

  Name:         spi.c
  Created by:   Stefan Ritt

  Contents:     SPI interface

\********************************************************************/

#include <stdio.h>
#include <string.h>
#include <intrins.h>
#include <stdlib.h>
#include "mscbemb.h"
#include "mpdc.h"

/*------------------------------------------------------------------*/

sbit SPI_CS_GPIO1 = P1 ^ 1;
sbit SPI_CS_GPIO2 = P1 ^ 4;
sbit SPI_CS_ADC1  = P1 ^ 2;
sbit SPI_CS_ADC2  = P1 ^ 3;
sbit SPI_CS_ADC3  = P1 ^ 5;
sbit SPI_CS_ADC4  = P1 ^ 6;
sbit SPI_CS_DISP  = P2 ^ 1;

sbit SPI_CLK      = P0 ^ 0;
sbit SPI_DI       = P0 ^ 1;
sbit SPI_DO       = P0 ^ 2;

void spi_init()
{
   SFRPAGE = LEGACY_PAGE;
   
   SPI_CLK      = 0;
   SPI_DO       = 0;
   SPI_CS_GPIO1 = 1;
   SPI_CS_GPIO2 = 1;
   SPI_CS_ADC1  = 1;
   SPI_CS_ADC2  = 1;
   SPI_CS_ADC3  = 1;
   SPI_CS_ADC4  = 1;
   SPI_CS_DISP  = 1;
}

void spi_adr(unsigned char adr)
/* set CS according to adr */
{
   SFRPAGE = LEGACY_PAGE;

   switch (adr) {
		 case ADR_GPIO1: SPI_CS_GPIO1 = 0; break;
		 case ADR_GPIO2: SPI_CS_GPIO2 = 0; break;
		 case ADR_ADC1:  SPI_CS_ADC1  = 0; break;
		 case ADR_ADC2:  SPI_CS_ADC2  = 0; break;
		 case ADR_ADC3:  SPI_CS_ADC3  = 0; break;
		 case ADR_ADC4:  SPI_CS_ADC4  = 0; break;
		 case ADR_DISP:  SPI_CS_DISP  = 0; break;
	 }
}

void spi_deadr()
/* remove all CS */
{
   SFRPAGE = LEGACY_PAGE;

   SPI_CS_GPIO1 = 1;
	SPI_CS_GPIO2 = 1;
	SPI_CS_ADC1  = 1;
	SPI_CS_ADC2  = 1;
	SPI_CS_ADC3  = 1;
	SPI_CS_ADC4  = 1;
   SPI_CS_DISP  = 1;
}

void spi_write_lsb(unsigned char d)
/* write one byte to SPI, LSB first */
{
   unsigned char i;

   SFRPAGE = LEGACY_PAGE;
  
   for (i=0 ; i<8 ; i++) {
      SPI_DO = (d & 0x01) > 0;
      d >>= 1;
      SPI_CLK = 1;
      SPI_CLK = 1;
      SPI_CLK = 1; // make pulse 120 ns wide
      SPI_CLK = 0;
   }
   SPI_DO = 0;
}

void spi_write_msb(unsigned char d)
/* write one byte to SPI, LSB first */
{
   unsigned char i;

   SFRPAGE = LEGACY_PAGE;
  
   for (i=0 ; i<8 ; i++) {
      SPI_DO = (d & 0x80) > 0;
      d <<= 1;
      SPI_CLK = 1;
      SPI_CLK = 1;
      SPI_CLK = 1; // make pulse 120 ns wide
      SPI_CLK = 0;
   }
   SPI_DO = 0;
}

unsigned char spi_read_msb()
{
   unsigned char i;
   unsigned char b;
   
   SFRPAGE = LEGACY_PAGE;
   b = 0;
   
   SPI_DO = 0;
   for (i=0 ; i<8 ; i++) {
      b = (b << 1) | SPI_DI;
      SPI_CLK = 1;
      SPI_CLK = 1;
      SPI_CLK = 1; // make pulse 120 ns wide
      SPI_CLK = 0;
   }
   
   return b;
}