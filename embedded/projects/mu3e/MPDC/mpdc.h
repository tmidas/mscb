/********************************************************************\

  Name:         mpdc.h
  Created by:   Stefan Ritt


  Contents:     Header file for Mu3e Power Distributor and Controller

\********************************************************************/

#define ADR_NO_CS 0
#define ADR_GPIO1 1
#define ADR_GPIO2 2
#define ADR_ADC1  3
#define ADR_ADC2  4
#define ADR_ADC3  5
#define ADR_ADC4  6
#define ADR_DISP  7

#define ERROR_TEMP         (1<<0)
#define ERROR_OVERCURRENT  (1<<1)

// spi.c
void spi_init(void);
void spi_adr(unsigned char adr);
void spi_deadr();
void spi_write_lsb(unsigned char d);
void spi_write_msb(unsigned char d);
unsigned char spi_read_msb();

// led.c
void led_init(void);
void led_puts(char *s);

// menu.c
void menu_yield(void);

// gpio.c
void gpio_init(void);
void gpio_write(unsigned char device, unsigned char adr, unsigned char d);
unsigned char gpio_read(unsigned char device, unsigned char adr);
void gpio_channel_on(unsigned char channel, unsigned char flag);
void gpio_get_status(unsigned char *s);

// monitor.c
void monitor_init();
float monitor_read_adc(unsigned char index, unsigned char channel);

// global user data structure
typedef struct {
   unsigned char chn[32];
   float current[32];
   float temp[4];
   unsigned char power;
   unsigned char error;
   float current_offset[32];
} USER_DATA;
