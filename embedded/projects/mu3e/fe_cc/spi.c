/********************************************************************\

  Name:         spi.c
  Created by:   Stefan Ritt

  Contents:     SPI interface

\********************************************************************/

#include <stdio.h>
#include <string.h>
#include <intrins.h>
#include <stdlib.h>
#include "mscbemb.h"
#include "fe_cc.h"

/*------------------------------------------------------------------*/

sbit ADR0 = P6 ^ 0;
sbit ADR1 = P6 ^ 1;
sbit ADR2 = P6 ^ 2;

sbit SPI_CLK = P5 ^ 3;
sbit SPI_DO  = P5 ^ 1;
sbit SPI_DI  = P5 ^ 0;
sbit SPI_CS  = P5 ^ 2;

sbit BACKPLANE_SPI_CLK = P6 ^ 4;
sbit BACKPLANE_SPI_DO  = P6 ^ 6;
sbit BACKPLANE_SPI_DI  = P6 ^ 7;
sbit BACKPLANE_SPI_CS  = P6 ^ 3;

void spi_init()
{
   SFRPAGE = CONFIG_PAGE;
   ADR0 = 0;
   ADR1 = 0;
   ADR2 = 0;
   SPI_CLK = 1;
   SPI_DO  = 1;
   SPI_CS  = 1;

   BACKPLANE_SPI_CLK = 0;
   BACKPLANE_SPI_DO  = 0;
   BACKPLANE_SPI_CS  = 1;
}

void spi_adr(unsigned char adr, unsigned char cs)
/* set address through 74HC138 decoder and CS line */
{
   SFRPAGE = CONFIG_PAGE;
   if (cs == 0) {
      SPI_DO = 0;
      ADR0 = adr & 0x01;
      adr >>= 1;
      ADR1 = adr & 0x01;
      adr >>= 1;
      ADR2 = adr & 0x01;
   }

   SPI_CS = cs;
}

void spi_write_lsb(unsigned char d)
/* write one byte to SPI, LSB first */
{
   unsigned char i;

   SFRPAGE = CONFIG_PAGE;
   for (i=0 ; i<8 ; i++) {
      SPI_DO = (d & 0x01) > 0;
      d >>= 1;
      SPI_CLK = 1;
      SPI_CLK = 1;
      SPI_CLK = 1; // make pulse 120 ns wide
      SPI_CLK = 0;
   }
   SPI_DO = 0;
}

void spi_write_msb(unsigned char d)
/* write one byte to SPI, MSB first */
{
   unsigned char data r;
   SFRPAGE = CONFIG_PAGE;

   r = d;
   SPI_DO = (r & 0x80);
   r <<= 1;
   SPI_CLK = 1;
   SPI_CLK = 1;
   SPI_CLK = 1; // make pulse 120 ns wide
   SPI_CLK = 0;

   SPI_DO = (r & 0x80);
   r <<= 1;
   SPI_CLK = 1;
   SPI_CLK = 1;
   SPI_CLK = 1; // make pulse 120 ns wide
   SPI_CLK = 0;

   SPI_DO = (r & 0x80);
   r <<= 1;
   SPI_CLK = 1;
   SPI_CLK = 1;
   SPI_CLK = 1; // make pulse 120 ns wide
   SPI_CLK = 0;

   SPI_DO = (r & 0x80);
   r <<= 1;
   SPI_CLK = 1;
   SPI_CLK = 1;
   SPI_CLK = 1; // make pulse 120 ns wide
   SPI_CLK = 0;

   SPI_DO = (r & 0x80);
   r <<= 1;
   SPI_CLK = 1;
   SPI_CLK = 1;
   SPI_CLK = 1; // make pulse 120 ns wide
   SPI_CLK = 0;

   SPI_DO = (r & 0x80);
   r <<= 1;
   SPI_CLK = 1;
   SPI_CLK = 1;
   SPI_CLK = 1; // make pulse 120 ns wide
   SPI_CLK = 0;

   SPI_DO = (r & 0x80);
   r <<= 1;
   SPI_CLK = 1;
   SPI_CLK = 1;
   SPI_CLK = 1; // make pulse 120 ns wide
   SPI_CLK = 0;

   SPI_DO = (r & 0x80);
   r <<= 1;
   SPI_CLK = 1;
   SPI_CLK = 1;
   SPI_CLK = 1; // make pulse 120 ns wide
   SPI_CLK = 0;

   SPI_DO = 0;
}

unsigned char spi_read_msb()
{
   unsigned char b;

   SFRPAGE = CONFIG_PAGE;
   b = 0;

   b = (b << 1) | SPI_DI;
   SPI_CLK = 1;
   SPI_CLK = 1;
   SPI_CLK = 1; // make pulse 120 ns wide
   SPI_CLK = 0;

   b = (b << 1) | SPI_DI;
   SPI_CLK = 1;
   SPI_CLK = 1;
   SPI_CLK = 1; // make pulse 120 ns wide
   SPI_CLK = 0;

   b = (b << 1) | SPI_DI;
   SPI_CLK = 1;
   SPI_CLK = 1;
   SPI_CLK = 1; // make pulse 120 ns wide
   SPI_CLK = 0;

   b = (b << 1) | SPI_DI;
   SPI_CLK = 1;
   SPI_CLK = 1;
   SPI_CLK = 1; // make pulse 120 ns wide
   SPI_CLK = 0;

   b = (b << 1) | SPI_DI;
   SPI_CLK = 1;
   SPI_CLK = 1;
   SPI_CLK = 1; // make pulse 120 ns wide
   SPI_CLK = 0;

   b = (b << 1) | SPI_DI;
   SPI_CLK = 1;
   SPI_CLK = 1;
   SPI_CLK = 1; // make pulse 120 ns wide
   SPI_CLK = 0;

   b = (b << 1) | SPI_DI;
   SPI_CLK = 1;
   SPI_CLK = 1;
   SPI_CLK = 1; // make pulse 120 ns wide
   SPI_CLK = 0;

   b = (b << 1) | SPI_DI;
   SPI_CLK = 1;
   SPI_CLK = 1;
   SPI_CLK = 1; // make pulse 120 ns wide
   SPI_CLK = 0;

   return b;
}

void spi_write8_lsb(unsigned char adr, unsigned char d)
{
   spi_adr(adr, 0);
   spi_write_lsb(d);
   spi_adr(0, 1);
}

void spi_write8_msb(unsigned char adr, unsigned char d)
{
   spi_adr(adr, 0);
   spi_write_msb(d);
   spi_adr(0, 1);
}

void backplane_spi_cs(unsigned char cs)
{
   SFRPAGE = CONFIG_PAGE;
   BACKPLANE_SPI_CS = cs;
}

void backplane_spi_write_lsb(unsigned char d)
/* write one byte to SPI, LSB first */
{
   unsigned char i;

   SFRPAGE = CONFIG_PAGE;
   for (i=0 ; i<8 ; i++) {
      BACKPLANE_SPI_DO = (d & 0x01) > 0;
      d >>= 1;
      BACKPLANE_SPI_CLK = 1;
      BACKPLANE_SPI_CLK = 1;
      BACKPLANE_SPI_CLK = 1; // make pulse 120 ns wide
      BACKPLANE_SPI_CLK = 0;
   }
   BACKPLANE_SPI_DO = 0;
}

void backplane_spi_write_msb(unsigned char d)
/* write one byte to SPI, MSB first */
{
   unsigned char data r;
   SFRPAGE = CONFIG_PAGE;

   r = d;
   BACKPLANE_SPI_DO = (r & 0x80);
   r <<= 1;
   BACKPLANE_SPI_CLK = 1;
   BACKPLANE_SPI_CLK = 1;
   BACKPLANE_SPI_CLK = 1; // make pulse 120 ns wide
   BACKPLANE_SPI_CLK = 0;

   BACKPLANE_SPI_DO = (r & 0x80);
   r <<= 1;
   BACKPLANE_SPI_CLK = 1;
   BACKPLANE_SPI_CLK = 1;
   BACKPLANE_SPI_CLK = 1; // make pulse 120 ns wide
   BACKPLANE_SPI_CLK = 0;

   BACKPLANE_SPI_DO = (r & 0x80);
   r <<= 1;
   BACKPLANE_SPI_CLK = 1;
   BACKPLANE_SPI_CLK = 1;
   BACKPLANE_SPI_CLK = 1; // make pulse 120 ns wide
   BACKPLANE_SPI_CLK = 0;

   BACKPLANE_SPI_DO = (r & 0x80);
   r <<= 1;
   BACKPLANE_SPI_CLK = 1;
   BACKPLANE_SPI_CLK = 1;
   BACKPLANE_SPI_CLK = 1; // make pulse 120 ns wide
   BACKPLANE_SPI_CLK = 0;

   BACKPLANE_SPI_DO = (r & 0x80);
   r <<= 1;
   BACKPLANE_SPI_CLK = 1;
   BACKPLANE_SPI_CLK = 1;
   BACKPLANE_SPI_CLK = 1; // make pulse 120 ns wide
   BACKPLANE_SPI_CLK = 0;

   BACKPLANE_SPI_DO = (r & 0x80);
   r <<= 1;
   BACKPLANE_SPI_CLK = 1;
   BACKPLANE_SPI_CLK = 1;
   BACKPLANE_SPI_CLK = 1; // make pulse 120 ns wide
   BACKPLANE_SPI_CLK = 0;

   BACKPLANE_SPI_DO = (r & 0x80);
   r <<= 1;
   BACKPLANE_SPI_CLK = 1;
   BACKPLANE_SPI_CLK = 1;
   BACKPLANE_SPI_CLK = 1; // make pulse 120 ns wide
   BACKPLANE_SPI_CLK = 0;

   BACKPLANE_SPI_DO = (r & 0x80);
   r <<= 1;
   BACKPLANE_SPI_CLK = 1;
   BACKPLANE_SPI_CLK = 1;
   BACKPLANE_SPI_CLK = 1; // make pulse 120 ns wide
   BACKPLANE_SPI_CLK = 0;

   BACKPLANE_SPI_DO = 0;
}

unsigned char backplane_spi_read_msb()
{
   unsigned char b;

   SFRPAGE = CONFIG_PAGE;
   b = 0;

   b = (b << 1) | BACKPLANE_SPI_DI;
   BACKPLANE_SPI_CLK = 1;
   BACKPLANE_SPI_CLK = 1;
   BACKPLANE_SPI_CLK = 1; // make pulse 120 ns wide
   BACKPLANE_SPI_CLK = 0;

   b = (b << 1) | BACKPLANE_SPI_DI;
   BACKPLANE_SPI_CLK = 1;
   BACKPLANE_SPI_CLK = 1;
   BACKPLANE_SPI_CLK = 1; // make pulse 120 ns wide
   BACKPLANE_SPI_CLK = 0;

   b = (b << 1) | BACKPLANE_SPI_DI;
   BACKPLANE_SPI_CLK = 1;
   BACKPLANE_SPI_CLK = 1;
   BACKPLANE_SPI_CLK = 1; // make pulse 120 ns wide
   BACKPLANE_SPI_CLK = 0;

   b = (b << 1) | BACKPLANE_SPI_DI;
   BACKPLANE_SPI_CLK = 1;
   BACKPLANE_SPI_CLK = 1;
   BACKPLANE_SPI_CLK = 1; // make pulse 120 ns wide
   BACKPLANE_SPI_CLK = 0;

   b = (b << 1) | BACKPLANE_SPI_DI;
   BACKPLANE_SPI_CLK = 1;
   BACKPLANE_SPI_CLK = 1;
   BACKPLANE_SPI_CLK = 1; // make pulse 120 ns wide
   BACKPLANE_SPI_CLK = 0;

   b = (b << 1) | BACKPLANE_SPI_DI;
   BACKPLANE_SPI_CLK = 1;
   BACKPLANE_SPI_CLK = 1;
   BACKPLANE_SPI_CLK = 1; // make pulse 120 ns wide
   BACKPLANE_SPI_CLK = 0;

   b = (b << 1) | BACKPLANE_SPI_DI;
   BACKPLANE_SPI_CLK = 1;
   BACKPLANE_SPI_CLK = 1;
   BACKPLANE_SPI_CLK = 1; // make pulse 120 ns wide
   BACKPLANE_SPI_CLK = 0;

   b = (b << 1) | BACKPLANE_SPI_DI;
   BACKPLANE_SPI_CLK = 1;
   BACKPLANE_SPI_CLK = 1;
   BACKPLANE_SPI_CLK = 1; // make pulse 120 ns wide
   BACKPLANE_SPI_CLK = 0;

   return b;
}
