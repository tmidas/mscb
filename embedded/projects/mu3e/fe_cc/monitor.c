/********************************************************************\

  Name:         monitor.c
  Created by:   Stefan Ritt


  Contents:     Driver for MAX1253 power monitor

\********************************************************************/

#include <stdio.h>
#include <string.h>
#include <intrins.h>
#include <stdlib.h>
#include <math.h>
#include "mscbemb.h"
#include "fe_cc.h"

#define CMD_MAN_CONV            0x00
#define CMD_READ_ALARM_REG      0x01
#define CMD_READ_DATA_REG       0x02
#define CMD_READ_ALL_DATA_REG   0x03
#define CMD_READ_CONF_REG       0x04
#define CMD_READ_GLOB_CONF      0x05
#define CMD_RESET               0x07
#define CMD_CLEAR_ALARM         0x08
#define CMD_CLEAR_ALL_ALARM     0x09
#define CMD_WRITE_DATA_REG      0x0a
#define CMD_WRITE_ALL_DATA_REG  0x0b
#define CMD_WRITE_CONF_REG      0x0c
#define CMD_WRITE_GLOB_CONF     0x0d

/*------------------------------------------------------------------*/

/* read 16 bits from MAX1253 power monitor */
void monitor_read(unsigned char cmd, unsigned char addr, unsigned char *pd, unsigned char nbytes)
{
   unsigned char i;
   
   spi_adr(SPI_ADR_ADC, 0);
   spi_write_msb((cmd << 4) | (addr & 0x0F));
   for (i=0 ; i<nbytes ; i++)
      *(pd++) = spi_read_msb();
   spi_adr(0, 1);
}

/*------------------------------------------------------------------*/

/* address certain register in MAX1253 power monitor */
void monitor_address(unsigned char cmd, unsigned char addr)
{
   spi_adr(SPI_ADR_ADC, 0);
   spi_write_msb((cmd << 4) | (addr & 0x0F));
}

/*------------------------------------------------------------------*/

void monitor_deaddress()
{
   spi_adr(0, 1);
}

/*------------------------------------------------------------------*/

/* write single byte to MAX1253 power monitor */
void monitor_write(unsigned char d)
{
   spi_write_msb(d);
}

/*------------------------------------------------------------------*/

void monitor_init()
{
   monitor_address(0x07, 0); // Reset

   monitor_address(CMD_WRITE_GLOB_CONF, 0); // Write global configuration registers
   monitor_write(0xFF); // Enable all channels
   monitor_write(0xFF);
   monitor_write(0x00); // Single ended input
   monitor_write(0x00);
   monitor_write(0x15); // INT push-pull active low, auto scan, internal reference
   monitor_deaddress();
}

/*------------------------------------------------------------------*/

float monitor_read_adc(unsigned char channel)
{
   unsigned short d;
   float v;
   
   monitor_read(CMD_READ_DATA_REG, channel, (char *)&d, 2); // Read data register
   
   v = (d >> 4)*2.5/4096.0; // convert to V with a 2.5 V reference
   
   switch (channel) {
      case MON_TEMP: 
         v *= 0.125/2.5*4096.0;
         v = floor(v*10+0.5)/10.0; // round to one digit
         break;
      case MON_VDD:  
         v *= 2.0;
         break;
      case MON_24V:
         v /= 2.0/(18.0+2.0);   
         break;
      case MON_3_3V:
         v /= 1.2/(0.45+1.2);   
         break;
      case MON_5V:
         v /= 1.2/(1.3+1.2);
         break;
   }
   
   // round to two digits
   v = floor(v*100+0.5)/100.0;
   return v;
}

