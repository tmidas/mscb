/********************************************************************\

  Name:         DS28EA00.h
  Created by:   Stefan Ritt

  Contents:     DS28EA00 1-wire sensor header file

\********************************************************************/

#define DS28EA00_MAX_N_DEVICES 16

unsigned char DS28EA00_scan(unsigned char i);
void DS28EA00_read(float *ptemp);
