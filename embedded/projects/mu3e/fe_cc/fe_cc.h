/********************************************************************\

  Name:         fe_cc.h
  Created by:   Stefan Ritt

  Contents:     Header file for fe_cc.c

\********************************************************************/

#ifndef FE_CC_H
#define FE_CC_H

// undefine this if there is no PHY chip soldered on the board
// define this if you havea PHY and want to use Ethernet
#define HAVE_ETHERNET

typedef struct {  // application data
	 unsigned char status;

   float voltage[3];
   float temp;

   unsigned char board_power[16];
	
   unsigned char max_config;
	unsigned char bp_mode_sel;
	unsigned char attention;

   unsigned char spi_slot;
   
   float         ow_temp[16];
   unsigned char ow_adr[16][8];
   
} USER_DATA;

#define ERROR_FAN1      1
#define ERROR_FAN2      2
#define ERROR_FAN3      4
#define ERROR_T0        8
#define ERROR_T1       16
#define ERROR_T2       32
#define ERROR_T3       64

typedef struct {  // submaster confiuguration
   char           host_name[20];
   char           password[20];
   unsigned char  eth_mac_addr[6];
   unsigned short magic;
} SUBM_CFG;

typedef struct {  // client configuration
   unsigned int node_addr;
   unsigned int group_addr;
   char node_name[16];
   unsigned short magic;
} CLI_CFG;

/*---- cmb.c ----*/
void flash_write_data(void);

/*---- submaster.c ----*/
void submaster_init(void);
void submaster_yield(void);
void client_yield(void);

/*---- spi.c ----*/
#define SPI_ADR_ADC        0
#define SPI_ADR_SR         1
#define SPI_ADR_RST_SR     7

#define WD_SPI_WRITE8      0x11
#define WD_SPI_WRITE16     0x12
#define WD_SPI_WRITE32     0x14
#define WD_SPI_READ8       0x21
#define WD_SPI_READ16      0x22
#define WD_SPI_READ32      0x24
#define WD_SPI_BOARD_ID    0x30
#define WD_SPI_CLEAR       0x40
#define WD_SPI_SYNC        0x50
#define WD_SPI_RESET       0xF0

void spi_init();
void spi_adr(unsigned char adr, unsigned char cs);
void spi_write_lsb(unsigned char d);
void spi_write_msb(unsigned char d);
void spi_write8_lsb(unsigned char adr, unsigned char d);
unsigned char spi_read_msb();
void backplane_spi_cs(unsigned char cs);
void backplane_spi_write_lsb(unsigned char d);
void backplane_spi_write_msb(unsigned char d);
unsigned char backplane_spi_read_msb();

/*---- gpio.c ----*/
#define GPIO_SW_PREV  (1<<3)
#define GPIO_SW_NEXT  (1<<2)
#define GPIO_SW_INC   (1<<1)
#define GPIO_SW_DEC   (1<<0)

void gpio_init();
void gpio_write(unsigned char device, unsigned char adr, unsigned char d);
unsigned char gpio_read(unsigned char device, unsigned char port);
void gpio_out(unsigned char device, unsigned char port, unsigned char d);
unsigned char gpio_in(unsigned char device, unsigned char port);
void gpio_board_select(unsigned char b);
void gpio_board_power(unsigned char p[16]);
void gpio_max_config_select(unsigned char flag);
void gpio_reset_fpga();
void gpio_reset_cpu();
void gpio_reset_max();
void gpio_bp_mode_sel(unsigned char mode);
void gpio_attention(unsigned char mode);

/*---- monitor.c ----*/
#define MON_TEMP  0
#define MON_VDD   1
#define MON_24V   3
#define MON_3_3V  4
#define MON_5V    5

void monitor_init();
float monitor_read_adc(unsigned char channel);
unsigned long monitor_read_alarm_reg();
void monitor_clear_alarms();
void monitor_set_alarm_limits(unsigned char channel, float lower, float upper);

/*---- upgrade.c ----*/
void upgrade(void);

#endif // CMB_H
