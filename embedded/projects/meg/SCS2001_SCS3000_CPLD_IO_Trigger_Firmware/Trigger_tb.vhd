library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity trigger_tb is
end trigger_tb;

ARCHITECTURE behavior OF trigger_tb IS 

  component Trigger is
  generic ( CGN_DATA_WIDTH: natural := 32 );
  port
	(
    -- clock
    CLK     : in std_logic;
    CLK_SEL : out std_logic_vector(3 downto 0);
	
    -- SPI interface; Mode CPOL = 0, CPHA = 0, MSB frist
    SCLK    : in  std_logic;
    SS      : in  std_logic;
    MOSI    : in  std_logic;
    MISO    : out std_logic;

    -- trigger ouput
    TRIG : out std_logic
   );  
end component;

  constant C_CLOCK : integer := 100_000_000;
  constant C_DATA_WIDTH : integer := 32;

  signal sclk :  std_logic := '0';
  signal ss   :  std_logic := '1';
  signal mosi :  std_logic := '0';
  signal miso : std_logic := '0';
  signal clk  :  std_logic := '0';
  signal clk_sel : std_logic_vector( 3 downto 0 );
  signal trig : std_logic;

  signal rxdat :  std_logic_vector(C_DATA_WIDTH-1 downto 0) := (others=>'0');
  signal txdat :  std_logic_vector(C_DATA_WIDTH-1 downto 0) := std_logic_vector(to_unsigned(99,C_DATA_WIDTH));
  
  --Procedures
  procedure do_spi (signal rxdat :  inout std_logic_vector(C_DATA_WIDTH-1 downto 0);
                    signal txdat :  inout std_logic_vector(C_DATA_WIDTH-1 downto 0);
                    signal SCLK  :  out std_logic;
                    signal SS    :  out std_logic;
                    signal MOSI  :  out std_logic;
                    SIGNAL MISO  :  in  std_logic
                    ) 
  is 
  begin
    MOSI <= '1';
    SS   <= '1';
    SCLK <= '0';
    wait for 500 ns;
    SS   <= '0';
    MOSI <= txdat(txdat'left);
    wait for 100 ns;
    for I in txdat'range loop
        wait for 100 ns;
        SCLK <= '1';                                -- steigende Flanke
        rxdat<= rxdat(rxdat'left-1 downto 0) & MISO;-- MISO einlesen
        txdat<= txdat(txdat'left-1 downto 0) & '0';  
        wait for 100 ns;
        SCLK <= '0';                                -- fallende Flanke
        MOSI <= txdat(txdat'left);                  -- MOSI aktualisieren
    end loop;   
    wait for 200 ns;
    SS   <= '1';
    wait for 500 ns;
  end do_spi;

BEGIN

  uut: Trigger
  generic map( CGN_DATA_WIDTH => C_DATA_WIDTH )
  PORT MAP(
    CLK     => clk,
    CLK_SEL => clk_sel,
    SCLK    => sclk,
    SS      => ss,
    MOSI    => mosi,
    MISO    => miso,
    TRIG    => trig
    );

  tb : PROCESS
  BEGIN
    
    wait for 1 us;
    txdat <= std_logic_vector(to_unsigned(999,C_DATA_WIDTH));
    do_spi( SCLK=>SCLK, SS=>SS, MOSI=>MOSI, MISO => miso, txdat=>txdat, rxdat => rxdat );

    wait for 100 us;
    txdat <= (others=>'1');
    do_spi( SCLK=>SCLK, SS=>SS, MOSI=>MOSI, MISO => miso, txdat=>txdat, rxdat => rxdat );

    wait for 100 us;
    txdat <= std_logic_vector(to_unsigned(999,C_DATA_WIDTH));
    do_spi( SCLK=>SCLK, SS=>SS, MOSI=>MOSI, MISO => miso, txdat=>txdat, rxdat => rxdat );

    wait for 100 us;
    txdat <= std_logic_vector(to_unsigned(999,C_DATA_WIDTH));
    do_spi( SCLK=>SCLK, SS=>SS, MOSI=>MOSI, MISO => miso, txdat=>txdat, rxdat => rxdat );

  wait; -- will wait forever
  END PROCESS;


  tb_clk : process
  begin
    clk <= '0';
    loop
      wait for 1 sec / C_CLOCK / 2;
      clk <= not clk;
    end loop;
  end process;

END;